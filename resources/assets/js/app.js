// fullpage
import 'vue-fullpage/vue-fullpage.css'
import VueFullpage from 'vue-fullpage'
Vue.use(VueFullpage);

require('./bootstrap');

window.Vue = require('vue');

// jquery
window.$ = window.jQuery = require('jquery');

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
