@extends('layouts.default')
@section('title', 'feature')

@section('content')
    {{--<h1>feature</h1>--}}
    {{--<div id="slider" class="beer-slider" data-beer-label="before">
        <img src="/images/feature/before.png" alt="">
        <div class="beer-revhfkajui you shiyrahjk alt="data-beerslider"</div>
        <span>zheshi beerslider de toubu banner<span/>
    </div>--}}
    <div id="slider" class="beer-slider" data-beer-label="before">
        <img src="/images/feature/man-hold-beer.jpg" alt="Original man holding beer" style="width: 100%;">
        <div class="beer-reveal" data-beer-label="after">
            <img src="/images/feature/man-hold-beer-after1logo.jpg"
                 alt="Processed with logo and Lightroom presets" style="width: 100%">
        </div>
    </div>
    <canvas>

    </canvas>

@stop