@extends('layouts.default')

@section('content')
    @if(Auth::check())
        <div class="row">
            <div class="col-md-8">
                <section class="status_form">
                    @include('shared._status_form')
                </section>
                <h3>微博列表</h3>
                @include('shared._feed')
            </div>
            <aside class="col-md-4">
                <section class="user_info">
                    @include('shared._user_info', ['user' => Auth::user()])
                </section>
            </aside>
        </div>
    @else
        <div class="jumbotron" style="background-color: #000000">
            <canvas id="canvas" width="800" height="300"></canvas>
            <p>
                <a class="btn btn-lg btn-success" href="{{ route('signup') }}" role="button">现在注册</a>
            </p>
            {{--<img src="/images/wallhaven-664146.jpg" width="100%">--}}
            <div id="jsi-fountain-container" class="container"></div>
        </div>
    @endif
    {{--<canvas id="canvas" ></canvas>--}}
@stop

