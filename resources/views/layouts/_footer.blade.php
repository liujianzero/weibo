<div class="col-md-12">
    <footer class="footer">
        <small class="slogon">
            <img class="brand-icon" src="https://lccdn.phphub.org/uploads/images/201612/12/1/iq7WQc2iuW.png?imageView2/1/w/34/h/34">
            <a href="https://laravel-china.org/courses">
                刻意练习，每日精进
            </a>
        </small>
        <nav>
            <ul>
                <li><a href="{{ route('about') }}">关于</a></li>
            </ul>
        </nav>
    </footer>
</div>

<script>
    $(function () {
        var height = $(window).height(),
            main = $('main'),
            header = $('.header'),
            footer = $('.footer'),
            total = main.innerHeight() + header.innerHeight() + footer.innerHeight();

        if (total < height) {
            footer.css({'margin-top': (height - total) + 'px'});
        }
    });
</script>
