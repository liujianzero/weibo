<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'Zero Web')</title>
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/pointer.css">

        <script src="/css/font.css"></script>
        {{--<script src="/css/fountain.css"></script>--}}
        <script src="/css/rain.css"></script>
        {{--<script src="/css/slider.css"></script>--}}
        {{--<script src="/css/prism.css"></script>--}}
    </head>
    <body>
    @include('layouts._header')
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            @include('shared._messages')
            @yield('content')
            @include('layouts._footer')
        </div>
    </div>

    <script src="/js/app.js"></script>
    <script src="/js/font.js"></script>
    <script src="/js/rain.js"></script>
    {{--<script src="/js/beerslider.js"></script>--}}
    {{--<script src="/js/prism.js"></script>--}}

    {{--<script src="/js/pointer.js"></script>--}}
    {{--<script src="/js/fountain.js"></script>--}}

    </body>
</html>
